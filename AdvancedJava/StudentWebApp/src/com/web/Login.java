package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Login")
public class Login extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
			
		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='blue'>");
		out.print("<center>");
			
		if (emailId.equalsIgnoreCase("ch0tuu") && password.equals("2602")) {		
				
			//Calling HRHomePage (Servlet) 
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StuHomePage");
			requestDispatcher.forward(request, response);
				
		} else {			
			out.print("<h1 style='color:red'>Invalid Credentials</h1>");
				
			//Calling Login.html 
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
			requestDispatcher.include(request, response);
		}
			
		out.print("<center></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}