package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/StuHomePage")
public class StuHomePage extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		//To Showcase emailId
		String emailId = request.getParameter("emailId");
			
		out.print("<html>");
		out.print("<body bgcolor='lightyellow'>");				
			
		out.print("<form align='right'>");
		out.print("<a href='StuHomePage'>Home</a> &nbsp;");
		out.print("<a href='Login.html'>Logout</a>");
		out.print("</form>");
			
		//To Showcase emailId
		out.print("<h3>Welcome: " + emailId + "!</h3>");
			
		out.print("<center>");
		out.print("<h1 style='color:green'>Welcome to Student Home Page</h1>");	
		out.print("<center></body></html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}