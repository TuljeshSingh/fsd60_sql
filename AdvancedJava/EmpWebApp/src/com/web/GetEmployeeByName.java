package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetEmployeeByName")
public class GetEmployeeByName extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String empName = request.getParameter("empName");
		
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		
		EmployeeDao employeedao = new EmployeeDao();
		Employee employee = employeedao.getEmployeeByName(empName);
		
		out.print("<body bgcolor='lightblue' text='black'>");


		if(employee != null){
			//adding the employee object under request object
			request.setAttribute("employee", employee);
				
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetEmployeeByName.jsp");
			requestDispatcher.forward(request, response);
		}
		else{
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
			requestDispatcher.include(request, response);
			
			out.print("<h1 style='colour:red;'>");
			out.print("Employee Details Not Found!!!");
			out.print("</h1>");
		}
		out.print("</body>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}