package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetAllEmployees")
public class GetAllEmployees extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		EmployeeDao employeeDao = new EmployeeDao();
		List<Employee> employeeList = employeeDao.getAllEmployees();

		out.print("<body bgcolor='lightblue' text='black'>");

		

		if (employeeList != null) {

			request.setAttribute("employeeList", employeeList);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetAllEmployees.jsp");
			requestDispatcher.forward(request, response);

			

		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
			requestDispatcher.include(request, response);
			
			out.print("<h1 style='color:red'>Unable to Fetch Employee Records</h1>");
		}
		out.print("</body>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
