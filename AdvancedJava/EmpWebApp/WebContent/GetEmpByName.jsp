<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>GetEmpById</title>
</head>
<body>

	<jsp:include page="HRHomePage.jsp" />
	<br />

	<form action="GetEmployeeByName">

		<table align="center">
			<tr>
				<h1 align="center">Enter Employee Id to Fetch :</h1>
			</tr>
			<tr>
				<td>Employee Name :</td>
				<td><input type="text" name="empName"
					placeholder="Enter Employee Name" /></td>
			</tr>
			<tr>
				<td><button>Fetch</button></td>
			</tr>
		</table>

	</form>
</body>
</html>