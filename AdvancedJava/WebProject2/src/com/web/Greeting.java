package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/Greeting")
public class Greeting extends HttpServlet {
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");
//		String UserName = request.getParameter("UserName");
//		System.out.println("welcome "+UserName+"!");
		PrintWriter out = response.getWriter();
		
		String UserName = request.getParameter("UserName");
		out.println("<html>");
		out.println("<body bgcolor = 'lightgreen' text ='blue'> ");
		out.println("<h1>welcome " +UserName+"!");
		out.println("</body>");
		out.println("</html>");
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}