<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor="lightgreen" text="blue">

<jsp:include page="AdminHomePage.jsp" />

<form action="UpdateAccessorieAdmin" method="post">
		
		<table>
			<tr>
				<td>Accessories Id</td>
				<td><input type="text" name="prodId" value="${accessories.prodId}" readonly></td>
			</tr>
			<tr>
				<td>Accessories Name</td>
				<td><input type="text" name="prodName" value="${accessories.prodName}" ></td>
			</tr>
			<tr>
				<td>Accessories Type</td>
				<td><input type="text" name="prodType" value="${accessories.prodType}" ></td>
			</tr>
			
			<tr>
				<td>Manufacturer</td>
				<td><input type="text" name="manufacturer" value="${accessories.manufacturer}"></td>
			</tr>
			
			<tr>
				<td>Accessories Price</td>
				<td><input type="text" name="prodPrice" value="${accessories.prodPrice}"></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center"><input type="submit" value="Update Accessorie" /></td>
			</tr>
		</table>
		
	</form>

</body>
</html>