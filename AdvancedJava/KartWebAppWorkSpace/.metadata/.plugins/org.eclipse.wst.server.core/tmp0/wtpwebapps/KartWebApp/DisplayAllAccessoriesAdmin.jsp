<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<jsp:include page="AdminHomePage.jsp" />

	<center>
		<table border="2">
			<tr>
				<th>Product ID</th>
				<th>Product Name</th>
				<th>Product Type</th>
				<th>Manufacturer</th>
				<th>Product Price</th>
				<th colspan="2">Actions</th>
			</tr>
			
			<c:forEach var="accessorie" items="${accessoriesList}">
				<tr>
					<td>${accessorie.prodId}</td>
					<td>${accessorie.prodName}</td>
					<td>${accessorie.prodType}</td>
					<td>${accessorie.manufacturer}</td>
					<td>${accessorie.prodPrice}</td>
					<td><a href="EditAccessoriesServlet?prodId=${accessorie.prodId}">Edit</a></td>
					<td><a href="DeleteAccessoriesServlet?prodId=${accessorie.prodId}">Delete</a></td>
				</tr>
			</c:forEach>
			
		</table>	
	</center>
</body>
</html>


























