<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<jsp:include page="AdminHomePage.jsp" /> <br/>

	<center>
		<table border="2">
			<tr>
				<th>CustId</th>
				<th>Name</th>
				<th>Mobile</th>
				<th>EMail Id</th>
				<th>LoginId</th>
				<th>Status</th>
				<th colspan="2">Actions</th>
			</tr>
			
			<c:forEach var="customer" items="${customerList}">
				<tr>
					<td>${customer.custId}</td>
					<td>${customer.custName}</td>
					<td>${customer.mobile}</td>
					<td>${customer.emailId}</td>
					<td>${customer.loginId}</td>
					<td>${customer.status}</td>
					<td><a href="AccountActivation?custId=${customer.custId}&status=Blocked">Block</a></td>
					<td><a href="AccountActivation?custId=${customer.custId}&status=Active">Activate</a></td>
				</tr>
			</c:forEach>
			
		</table>	
	</center>


</body>
</html>