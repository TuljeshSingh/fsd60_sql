package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.CustomerDAO;
import com.dto.Customer;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		
		HttpSession session = request.getSession();
		session.setAttribute("loginId", loginId);
		
		if(loginId.equalsIgnoreCase("Admin") && password.equalsIgnoreCase("Admin")) {
			RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage.jsp");
			rd.forward(request, response);
		} else {
			
			CustomerDAO custDAO = new CustomerDAO();
			Customer customer = custDAO.getCustomer(loginId, password);
			
			if(customer != null && customer.getStatus().equalsIgnoreCase("Active")) {
				request.setAttribute("loginId", loginId);
				session.setAttribute("customer", customer);
				
				RequestDispatcher rd = request.getRequestDispatcher("CustomerHomePage.jsp");
				rd.forward(request, response);
			} else {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
	
				out.println("<body bgcolor='yellow' text='red'>");
				out.println("<center><h1>Invalid Credentials!!!</h1></center>");
				out.println("</body>");
				
				RequestDispatcher rd = request.getRequestDispatcher("Login.html");
				rd.include(request, response);
			}
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
