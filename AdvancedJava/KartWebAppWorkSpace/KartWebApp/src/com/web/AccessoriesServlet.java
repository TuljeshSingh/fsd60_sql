package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.AccessoriesDAO;
import com.dao.KartDAO;
import com.dto.Accessories;

@WebServlet("/AccessoriesServlet")
public class AccessoriesServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int prodId = Integer.parseInt(request.getParameter("prodId"));
		
		AccessoriesDAO accessoriesDAO = new AccessoriesDAO();				
		Accessories accessorie = accessoriesDAO.getAccessorie(prodId);
		
		HttpSession session = request.getSession();
		String loginId = (String) session.getAttribute("loginId");
		
		KartDAO kartDAO = new KartDAO();
		int result = kartDAO.addAccessorie(accessorie, loginId);
		
		if(result > 0) {	
			
			RequestDispatcher rd = request.getRequestDispatcher("DisplayAllAccessories");
			rd.forward(request, response);			
		}		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
