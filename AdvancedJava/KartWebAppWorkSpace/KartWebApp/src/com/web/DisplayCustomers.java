package com.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CustomerDAO;
import com.dto.Customer;

@WebServlet("/DisplayCustomers")
public class DisplayCustomers extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CustomerDAO customersDAO = new CustomerDAO();
		List<Customer> customerList = customersDAO.getCustomers();
		
		if(customerList != null) {
			request.setAttribute("customerList", customerList);
			RequestDispatcher rd = request.getRequestDispatcher("DisplayCustomers.jsp");
			rd.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
