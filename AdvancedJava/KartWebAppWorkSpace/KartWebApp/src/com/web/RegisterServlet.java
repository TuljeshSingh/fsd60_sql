package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CustomerDAO;
import com.dto.Customer;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String custName = request.getParameter("custName");
		String mobile = request.getParameter("mobile");
		String emailId = request.getParameter("emailId");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String status = "Active";
				
		Customer customer = new Customer(custName, mobile, emailId, loginId, password, status);
		
		CustomerDAO customerDAO = new CustomerDAO();
		int result = customerDAO.registerCustomer(customer);
		
		if(result > 0) {			
			int custId1 = customerDAO.getLatestCustomerId();
			
			out.println("<body bgcolor='lightgreen' text='blue'>");
			out.println("<center>");
			out.println("<h3>Registration Done Successfully!!!</h3>");
			out.println("<h3>Your Customer Id is " + custId1 +"</h3> <br/>");
			out.println("<h3>Click <a href='Login.html'>Here</a> to Signin</h3>");
			out.println("</center>");
			out.println("/<body>");						
		} else {
			out.println("<body bgcolor=yellow text=red>");
			out.println("<h1><center>Registration Failed!!!</center></h1>");
			out.println("</body>");			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");			
			requestDispatcher.include(request, response);
		}
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
