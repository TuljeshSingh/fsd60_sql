package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Accessories;

public class AccessoriesDAO {

	public List<Accessories> getAccessories() {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		con = DbConnection.getConnection();
		String SELECT = "Select * from Accessories";
		
		try {
			pst=con.prepareStatement(SELECT);
			rs = pst.executeQuery();
			
			List<Accessories> accessoriesList = new ArrayList<Accessories>();
			
			while(rs.next()) {
				Accessories accessories = new Accessories();
				
				accessories.setProdId(rs.getInt(1));
				accessories.setProdName(rs.getString(2));
				accessories.setProdType(rs.getString(3));
				accessories.setManufacturer(rs.getString(4));
				accessories.setProdPrice(rs.getDouble(5));
				
				accessoriesList.add(accessories);
			}
			
			return accessoriesList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public Accessories getAccessorie(int prodId) {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		con = DbConnection.getConnection();
		String SELECT = "Select * from Accessories where prodId=?";
		
		try {
			pst=con.prepareStatement(SELECT);
			pst.setInt(1, prodId);
			rs = pst.executeQuery();
			
			if(rs.next()) {
				Accessories accessorie = new Accessories();
				
				accessorie.setProdId(rs.getInt(1));
				accessorie.setProdName(rs.getString(2));
				accessorie.setProdType(rs.getString(3));
				accessorie.setManufacturer(rs.getString(4));
				accessorie.setProdPrice(rs.getDouble(5));
				
				return accessorie;
			}						
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public int deleteAccessorie(int prodId) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		
		con = DbConnection.getConnection();
		String DELETE = "Delete from Accessories where prodId=?";
		
		try {
			pst=con.prepareStatement(DELETE);
			pst.setInt(1, prodId);
			result = pst.executeUpdate();
			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

	public int updateAccessorie(Accessories accessorie) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		
		con = DbConnection.getConnection();
		String UPDATE = "Update Accessories set prodName=?, prodType=?, manufacturer=?, prodPrice=? where prodId=?";
		
		try {
			pst=con.prepareStatement(UPDATE);
			pst.setInt(5, accessorie.getProdId());
			pst.setString(1, accessorie.getProdName());
			pst.setString(2, accessorie.getProdType());
			pst.setString(3, accessorie.getManufacturer());
			pst.setDouble(4, accessorie.getProdPrice());
			result = pst.executeUpdate();
			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

	public int addAccessorie(Accessories accessorie) {
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		
		con = DbConnection.getConnection();
		String INSERT = "insert into accessories (prodName, prodType, manufacturer, prodPrice) values (?, ?, ?,?)";
		
		try {
			pst=con.prepareStatement(INSERT);
			pst.setString(1, accessorie.getProdName());
			pst.setString(2, accessorie.getProdType());
			pst.setString(3, accessorie.getManufacturer());
			pst.setDouble(4, accessorie.getProdPrice());
			result = pst.executeUpdate();
			
			return result;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	public int getLatestProductId() {
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		con = DbConnection.getConnection();
		String SELECT = "Select max(prodId) from Accessories";
		
		try {
			pst=con.prepareStatement(SELECT);
			rs = pst.executeQuery();
			
			if(rs.next()) {
				return rs.getInt(1);
			}						
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if(con != null) {
					rs.close();
					pst.close();
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}

}
