<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/css">
.logoutLabelPosition{
   position:fixed;
   right:10px;
   top:5px;
}
</script>

<title>Logout</title>
</head>
<body>
<form align="right">
  <label class="logoutLabelPosition">
  <a href="AdminHomePage.jsp">Home</a> &nbsp; &nbsp;
  <a href="Login.html">Logout</a>
  </label>
</form>
</body>
</html>