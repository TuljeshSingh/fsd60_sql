Adv.Java Day-07
---------------


Delete Employee
---------------

GetAllEmployee.jsp
------------------
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<!-- Using the JSTL Core Tag -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllEmployees</title>
</head>
<body>
	<jsp:include page="HRHomePage.jsp" />
	<br />
	<table border='2' align='center'>
		<tr>
			<th>EmpId</th>
			<th>EmpName</th>
			<th>Salary</th>
			<th>Gender</th>
			<th>Email-Id</th>
			<th colspan='2'>Actions</th>
		</tr>
		<c:forEach var="employee" items="${employeeList}">		
		<tr>
			<td> ${ employee.empId   } </td>
			<td> ${ employee.empName } </td>
			<td> ${ employee.salary  } </td>
			<td> ${ employee.gender  } </td>
			<td> ${ employee.emailId } </td>
			<td> Edit </td>
			<td> <a href='DeleteEmployee?empId=${employee.empId}'>Delete</a> </td>
		</tr>		
		</c:forEach>
	</table>
</body>
</html>




2. Create a Servlet called DeleteEmployee under com.web as:

DeleteEmployee
--------------
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
		
	int empId = Integer.parseInt(request.getParameter("empId"));
		
	EmployeeDao employeeDao = new EmployeeDao();
	int result = employeeDao.deleteEmployee(empId);
		
		
	if (result > 0) {
		request.getRequestDispatcher("GetAllEmployees").forward(request, response);
	} else {
		request.getRequestDispatcher("HRHomePage.jsp").include(request, response);
		out.print("<br/>");
		out.print("<h2 style='color:red;'>Unable to Delete the Employee Record!!!</h2>");
	}
		
}




3. Under EmployeeDao, add the method called deleteEmployee() as:

deleteEmployee(int empId)
-------------------------

public int deleteEmployee(int empId) {
	Connection connection = DbConnection.getConnection();
	PreparedStatement preparedStatement = null;
		
	String deleteQuery = "delete from employee where empId=?";
	
	try {
		preparedStatement = connection.prepareStatement(deleteQuery);
		preparedStatement.setInt(1, empId);
			
		return preparedStatement.executeUpdate();			
			
	} catch (SQLException e) {
		e.printStackTrace();
	}
		
	finally {
		try {
			if (connection != null) {
				preparedStatement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return 0;
}	



--Done with Delete Employee


--------------------------------------------------------------







EditEmployee
------------


GetAllEmployees.jsp
-------------------
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<!-- Using the JSTL Core Tag -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllEmployees</title>
</head>
<body>
	<jsp:include page="HRHomePage.jsp" />
	<br />
	<table border='2' align='center'>
		<tr>
			<th>EmpId</th>
			<th>EmpName</th>
			<th>Salary</th>
			<th>Gender</th>
			<th>Email-Id</th>
			<th colspan='2'>Actions</th>
		</tr>
		<c:forEach var="employee" items="${employeeList}">		
		<tr>
			<td> ${ employee.empId   } </td>
			<td> ${ employee.empName } </td>
			<td> ${ employee.salary  } </td>
			<td> ${ employee.gender  } </td>
			<td> ${ employee.emailId } </td>
			<td> <a href='EditEmployee?empId=${employee.empId}'>  Edit  </a> </td>
			<td> <a href='DeleteEmployee?empId=${employee.empId}'>Delete</a> </td>
		</tr>		
		</c:forEach>
	</table>
</body>
</html>





2. Create a servlet called EditEmployee under com.web as:

EditEmployee (Servlet)
----------------------
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
		
	int empId = Integer.parseInt(request.getParameter("empId"));
		
	EmployeeDao employeeDao = new EmployeeDao();
	Employee employee = employeeDao.getEmployeeById(empId);
		
	request.setAttribute("employee", employee);
	request.getRequestDispatcher("EditEmployee.jsp").include(request, response);
		
}




3. Create a jsp file called EditEmployee.jsp under webcontent as:

EditEmployee.jsp
----------------
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EditEmployee</title>
</head>
<body>

	<jsp:include page="HRHomePage.jsp" />
	<br/>
	
	<form action="UpdateEmployee" method="post">
	<table align="center">
		<tr>
			<td>Enter EmpId</td>
			<td><input type="text" name="empId" value="${employee.empId}" readonly/></td>
		</tr>
		<tr>
			<td>Enter EmpName</td>
			<td><input type="text" name="empName" value="${employee.empName}" /></td>
		</tr>
		<tr>
			<td>Enter Salary</td>
			<td><input type="text" name="salary" value="${employee.salary}" /></td>
		</tr>
		<tr>
			<td>Select Gender</td>
			<td>
				<select name="gender">
					<option value="${employee.gender}" selected>${employee.gender}</option>
					<option value="Male"   > Male   </option>
					<option value="Female" > Female </option>
					<option value="Others" > Others </option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Enter Email-Id</td>
			<td><input type="text" name="emailId" value="${employee.emailId}" readonly/></td>
		</tr>
		<tr>
			<td>Enter Password</td>
			<td><input type="password" name="password" value="${employee.password}" readonly/></td>
		</tr>
		<tr>
			<td></td>
			<td><button>Update Employe</button></td>
		</tr>
	</table>	
</form>

</body>
</html>







4. Create a Servlet called UpdateEmployee under com.web as:


UpdateEmployee (Servlet)
------------------------
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
		
	int empId = Integer.parseInt(request.getParameter("empId"));
	String empName = request.getParameter("empName");
	double salary = Double.parseDouble(request.getParameter("salary"));
	String gender = request.getParameter("gender");
	String emailId = request.getParameter("emailId");
	String password = request.getParameter("password");
		
	Employee employee = new Employee(empId, empName, salary, gender, emailId, password);
		
	EmployeeDao employeeDao = new EmployeeDao();
	int result = employeeDao.updateEmployee(employee);
		
	if (result > 0) {
		request.getRequestDispatcher("GetAllEmployees").forward(request, response);
	} else {
		request.getRequestDispatcher("HRHomePage.jsp").include(request, response);
		out.print("<br/>");
		out.print("<h3 style='color:red;'>Unable to Update Employee Record!!!</h3>");
	}
}





5. Add a method called updateEmployee(employee) under EmployeeDao class under com.dao

updateEmployee(Employee employee)
---------------------------------


public int updateEmployee(Employee employee) {
	Connection connection = DbConnection.getConnection();
	PreparedStatement preparedStatement = null;
		
	String updateQuery = "update employee set empName=?, salary=?, gender=? where empId=?";
		
	try {
		preparedStatement = connection.prepareStatement(updateQuery);
					
		preparedStatement.setString(1, employee.getEmpName());
		preparedStatement.setDouble(2, employee.getSalary());
		preparedStatement.setString(3, employee.getGender());
		preparedStatement.setInt(4, employee.getEmpId());
			
		return preparedStatement.executeUpdate();			
			
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		try {
			if (connection != null) {
				preparedStatement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		
	return 0;
}	
	



--Done with EditEmployee


--------------------------------------------------------------



Restoring the BackUp(war) file
------------------------------

1. Create a saperate workspace folder called (EmpWebAppBackUp) on desktop
2. under EmpWebAppBackUp folder create one more folder called workspace.
3. copy the war file ex: (EmpWebApp(JSP Final-2).war) into the EmpWebAppBackUp folder.
4. Open Eclipse and select the above workspace folder.
5. Go to File -> Import -> Scroll Down -> Web -> Select war file -> Next.
6. Click on Browse button and select the war file you have copied into the EmpWebAppBackUp folder
7. Dont Select any thing, Click on Next.
8. Dont Select any thing, click on  Finish.

9. Add Apache Tomcat Server to the Servers Tab.
10. Add Apache Tomcat to our Project.
11. Run the Project.



--------------------------------------------------------------













