import { Component } from '@angular/core';

@Component({
  selector: 'app-show-employees',
  templateUrl: './show-employees.component.html',
  styleUrl: './show-employees.component.css'
})
export class ShowEmployeesComponent {

  employees: any[] = [
    { empId: 1, empName: 'Praveen', salary: 50000, gender: 'Male', doj: '01-15-2023', country: 'USA', emailId: 'praveen@gmail.com', password: '123' },
    { empId: 2, empName: 'Tuljesh', salary: 60000, gender: 'Male', doj: '02-20-2022', country: 'Canada', emailId: 'Tuljesh@gmail.com', password: '123' },
    { empId: 3, empName: 'Prash', salary: 70000, gender: 'Male', doj: '03-25-2021', country: 'UK', emailId: 'Prash@gmail.com', password: '123' },
    { empId: 4, empName: 'Mahi', salary: 55000, gender: 'Male', doj: '04-30-2020', country: 'Australia', emailId: 'Mahi@gmail.com', password: '123' },
    { empId: 5, empName: 'Naveen', salary: 80000, gender: 'Male', doj: '05-10-2019', country: 'Germany', emailId: 'Naveen@gmail.com', password: '123' }
  ];


}
