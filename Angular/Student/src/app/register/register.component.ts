import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {

  employee : any;

  constructor(){
    this.employee = {
      empName :"",
      salary : "",
      gender: "",
      doj : "",
      country : "",
      emailId : "",
      password : "",
      deptId : ""
    }
  }
  registerSubmit(registerForm:any){
    this.employee.empName = registerForm.empName;
    this.employee.salary = registerForm.salary;
    this.employee.gender = registerForm.gender;
    this.employee.doj = registerForm.doj;
    this.employee.country = registerForm.country;
    this.employee.emailId = registerForm.emailId;
    this.employee.password = registerForm.password;
    this.employee.deptId = registerForm.deptId
    console.log(this.employee);
    alert("Registration Succsses...");
  }
}