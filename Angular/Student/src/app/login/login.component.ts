import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  emailId: any;
  password: any;

  constructor() { 
  }

  loginSubmit(loginForm: any) {
    console.log(loginForm);

    // Using loginForm as an argument under login.html
    //------------------------------------------------
    // if (loginForm.value.emailId == 'HR' && loginForm.value.password == 'HR') {
    //   alert('Login Success!');
    // } else {
    //   alert('Invalid Credentials');
    // }

    // Using loginForm.value as an argument under login.html
    //------------------------------------------------------
     if (loginForm.emailId == 'HR' && loginForm.password == 'HR') {
      alert('Login Success!');
    } else {
      alert('Invalid Credentials');
    }

  }

}
