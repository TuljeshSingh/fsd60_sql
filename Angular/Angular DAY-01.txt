Angular Day-01
**************
1st Version : AngularJS (JS - Java Script)
2nd Version : Angular (JS is replaced with TS (TypeScript))

Angular is a front-end application development
Angular is Introducted By Google
Angular uses TypeScript
Type Script is a superset of Java Script
Type Script is introduced by Microsoft
TypeScript --> Compile --> JavaScript

Angular is a component based application development.
Angular 1.0 Component (HTML, CSS, JavaScript)
Angular 2.0 Component (HTML, CSS, TypeScript)

Login    - Folder Login (Login.html, Login.css, Login.ts)
Register - Folder Register (Register.html, Register.css, Register.ts)

Key Features
------------
1. Responsive Web Application Development.
2. First UI Framework to support Dependency Injection.
3. SPA (Single Page Application)


--------------------------------------------------------

Installation
************

1. Install NodeJS (https://nodejs.org/en/download)
(LTS - Windows Installer - Windows x64)


Install Angular
---------------

1. Open Angular.io website and Navigate to 
Docs -> Getting Started -> Setup -> Scroll Down, 

You can see angular installation command: 
npm install -g @angular/cli

2. Open Node.JS Command Prompt
Paste the angular installation command (npm install -g @angular/cli) and hit enter to install angular.



--------------------------------------------------------





Angular Project Creation
************************

1. Under Node.js command prompt, navigate to the path where you want to create the Angular project.

Syntax:
-------
cd\
cd Paste_The_Path_Where_You_Want_To_Create_Angular_Project
(C:\1.TS\FSD\fsd60\5.Angular)


Example:
--------
C:\Users\Sree Harsha>cd\
C:\>cd C:\1.TS\FSD\fsd60\5.Angular
C:\1.TS\FSD\fsd60\5.Angular>



2. Create the Angular Project by the name called "Demo" as:
ng new Demo --standalone false

StyleSheet: CSS
ServerSideRendering: N

After Project is Created, You will be getting the following message and it is configured with the git configuation.
√ Packages installed successfully.
    Directory is already under version control. Skipping initialization of git.


If the project is configured with git, you will be getting the above message otherwise, you will be getting git related warnings, which can be ignored.


--Done with Angular Project Recation



--------------------------------------------------------


Run the Angular Project from NodeJS Command Prompt as:

1. Navigate to the Angular Project Folder: 
cd Demo

2. Provide the following command to compile and run the Angular project as:
ng serve --open


You can see angular project is compiled
Initial chunk files | Names         |  Raw size
polyfills.js        | polyfills     |  83.60 kB |
main.js             | main          |  23.11 kB |
styles.css          | styles        |  95 bytes |

                    | Initial total | 106.80 kB
Application bundle generation complete. [2.163 seconds]
Watch mode enabled. Watching for file changes...
  ➜  Local:   http://localhost:4200/
  ➜  press h + enter to show help




Run the Angular Project
***********************
1. Open the Browser, and provide the following url:
localhost:4200

here 4200 is the default port number to run the Angular Application.



-----------------------------------------------------------------


Stop the Angular Project Execution
----------------------------------

Click on the terminal or command prompt and hit (Ctrl + C) twice.

Exit the command prompt or terminal


-----------------------------------------------------------------

--Done with Angular Installation, Project Creation and Execution

-----------------------------------------------------------------



Open the Angular Project using the Visual Studio code:

File -> Open Folder -> Select Demo Project Folder and Click on OK.

Select the option I, trust the authors

View -> Terminal -> and select the command prompt rather than powershell
Then provide the command to compile and run the Angular application
ng serve --open


ng    - Angular
Serve - Serving (Compiling and Executing)
--    - Additional Arugment
Open  - Open in your system default browser


-----------------------------------------------------------------

Moving the Project from one computer to another


Under the Project, Select All files and folders except node_modules.
Zip the project and copy it.
Extract the project and open it using VS Code.
From the terminal, use the following command to install node_modules:
npm install


-----------------------------------------------------------------



Create a test component under VS Code using new command prompt:

ng g c test

ng   - angular
g    - generate
c    - component
test - Name of the Component


Test Component will be created: 
test
test-component.html
test-component.css
test-component.ts
test-component.spec.ts

files will created


-----------------------------------------------------------------


Once the Test Component is Created,

1. Open test.component.ts, copy the value of select i.e., app-test

2. Open app.component.html and paste the select value as a tag like:
<app-test></app-test>

3. You can see the output like: 
test works!


--Now your test component is executing

-----------------------------------------------------------------




Let us write some code under test component


constructor: will executes when the class is loaded
ngOnInit   : will executes when the component is loaded

constructor is used to initialise the class variables
constructor is used to implement the dependency injection

ngOnInit is used for api calls


Interpolation: {{ ts_variable }}

 

test.component.ts
-----------------
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {

  id: number;
  name: string;
  age: number;

  address: any;
  hobbies: any;

  constructor() {
    //alert('Constructor Invoked...');

    this.id = 101;
    this.name = 'Harsha';
    this.age = 22;

    this.address = {streetNo: 101, city: 'Hyd', state: 'Telangana'}
  
    this.hobbies = ['Running', 'Reading', 'Music', 'Movies', 'Eating'];
  }

  ngOnInit() {
    //alert('ngOnInit Invoked...');
  }

}



test.component.html
-------------------

id: {{id}} <br/>
name: {{name}} <br/>
age: {{age}} <br/><br/>

<h4><u>Address</u></h4>
StreetNo: {{address.streetNo}} <br/>
City: {{address.city}} <br/>
State: {{address.state}} <br/> <br/>

<h4><u>Hobbies</u></h4>
<ul>
    <li *ngFor="let hobby of hobbies">{{hobby}}</li>
</ul>

-----------------------------------------------------------------



