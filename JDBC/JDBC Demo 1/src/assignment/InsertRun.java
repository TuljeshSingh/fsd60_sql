package assignment;

import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class InsertRun {

	public static void main(String[] args) {
		Connection con = DbConnection.getConnection();
		Statement st = null;
		Scanner sc = new Scanner(System.in);
		int empId = sc.nextInt();
		String empName = sc.next();
		double salary = sc.nextDouble();
		String gender = sc.next();
		String email = sc.next();
		String password = sc.next();
		String insertQuery = "insert into employee values(" + empId + ", ' " + empName + " ', " + salary + ", ' " + gender + " ',' " + email + " ',' " + password + " ')";
		
		try {
			st = con.createStatement();
			// dml = executeupdate - insert, update, delete
			// dql = executequery
			//
			int res = st.executeUpdate(insertQuery);
			if(res > 0) {
				System.out.print(res);
			}else {
				System.out.print("failed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(con != null) {
			st.close();
			con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}