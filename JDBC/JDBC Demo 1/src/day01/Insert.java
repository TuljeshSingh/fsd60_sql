package day01;

import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.SQLException;

import java.sql.Statement;

import com.db.DbConnection;

public class Insert {

	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		
		Statement statement = null ;

		int empId = 110;

		String empName = "Dianaa";

		double salary = 95000.00;

		String gender = "Female";

		String emailId = "Dianaa@gmail.com";

		String password = "2204";

		String insertQuery = "insert into employee values(" +
		empId + ", '" + empName + "', " + salary + ", '" + gender
				+ "' , '" + emailId + "', '" + password + "')";
		
		try {
			
			statement = connection.createStatement();
			
			int result = statement.executeUpdate(insertQuery);
			
			if(result > 0){
				
				System.out.println(result+" Records Inserted...");
				
			}
			
			else{
				
				System.out.println("Record Insertion Failed...");
				
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		}
		
		try {
			
			if (connection != null) {
				
				statement.close();
				
				connection.close();
				
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		}
	}

}
