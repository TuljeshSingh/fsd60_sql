package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

//Get All Records from Employee Table
public class Fetch {
	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;

		String selectQuery = "select * from employee";

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);

			if (resultSet != null) {

				while(resultSet.next()) {

//					System.out.println("EmpId   : " + resultSet.getInt(1));
//					System.out.println("EmpName : " + resultSet.getString(2));
//					System.out.println("Salary  : " + resultSet.getDouble("salary"));
//					System.out.println("Gender  : " + resultSet.getString(4));
//					System.out.println("EmailId : " + resultSet.getString(5));
//					System.out.println("Password: " + resultSet.getString(6));
//					System.out.println();
					
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
