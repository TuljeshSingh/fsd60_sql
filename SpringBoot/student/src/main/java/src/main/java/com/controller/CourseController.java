package src.main.java.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CourseDao;
import com.model.Course;

@RestController
public class CourseController {

	@Autowired
	CourseDao courseDao;
	
	@GetMapping("getAllCourses")
	public List<Course> getAllCourses() {
		return courseDao.getAllCourses();
	}
	@GetMapping("getCourseById/{courseId}")
	public Course getCourseById(@PathVariable("courseId") int courseId) {
		return courseDao.getCourseById(courseId);
	}
	@GetMapping("getCourseByName/{courseName}")
	public Course getCourseByName(@PathVariable("courseName") String courseName) {
		return courseDao.getCourseByName(courseName);
	}
	
	@PostMapping("addCourse")
	public Course addCourse(@RequestBody Course course) {
		return courseDao.addCourse(course);
	}
	
	@PutMapping("updateCourse")
	public Course updateCourse(@RequestBody Course course) {
		return courseDao.updateCourse(course);
	}
	
	@DeleteMapping("deleteCourseById/{courseId}")
	public String deleteCourseById(@PathVariable("courseId") int courseId) {
		courseDao.deleteCourseById(courseId);
		return "Course with courseId: " + courseId + ", Deleted Successfully!!!";
	}
}
