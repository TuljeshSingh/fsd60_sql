SQL
---

create table employee (
empId int(4) primary key, 
empName varchar(20),
salary double(8, 2),
gender varchar(6),
doj date,
emailId varchar(20) unique key,
password varchar(20));
 
insert into employee values (101, 'Harsha',  1212.12, 'Male',   '2018-11-13', 'harsha@gmail.com',  '123');
insert into employee values (102, 'Pasha',   8989.89, 'Male',   '2017-10-14', 'pasha@gmail.com',   '123');
insert into employee values (103, 'Indira',  7878.78, 'Female', '2016-09-15', 'indira@gmail.com',  '123');
insert into employee values (104, 'Vamsi',   4545.45, 'Male',   '2015-08-16', 'vamsi@gmail.com',   '123');
insert into employee values (105, 'Deepika', 7979.79, 'Female', '2014-07-17', 'deepika@gmail.com', '123');
insert into employee values (106, 'Utkarsh', 6767.67, 'Male',   '2013-06-18', 'utkarsh@gmail.com', '123');

Select * from employee;

SQL Day-04
----------


Built-In Functions
------------------
1. String Functions
2. Numeric Functions
3. Date Functions


String functions
----------------
select * from employee;
select empName from employee;
select empname from employee where empId=101;
select length(empname) from employee where empId=101;
select empName, length(empname) from employee where empId=101;
select empName, length(empname) from employee;
select empName, length(empname) as 'Length of Name' from employee;





Built-In Functions
------------------



1. String Functions
-------------------
//ASCII
select empname,ASCII(Empname) from Employee1;

select empname,ASCII(Empname) ASCIIOFFIRSTCHARACTER from Employee1;
//LENGTH OF STRING
select empname,char_length(empname) as LENGTHOFNAME from Employee1;
select empname,character_length(empname) as LENGTHOFNAME from Employee1;
//CONCAT()
select empname,concat(empname," ",gender) from Employee1;
//INSERT()
select insert(empname,6,2,"Hello") from Employee1;
//REVERSE()
select empname,reverse(empname) from Employee1;

SELECT empName, UPPER(empName) AS uppercase_name FROM employee;
SELECT empName, LOWER(empName) AS lowercase_name FROM employee;
SELECT empName, LEFT(empName, 3) AS first_three_chars FROM employee;
SELECT empName, RIGHT(emailId, 4) AS last_four_chars FROM employee;
SELECT empName, TRIM(empName) AS trimmed_name FROM employee;
SELECT empName FROM employee WHERE empName LIKE 'H%';






2. Numeric Functions
--------------------
SELECT SUM(salary) AS total_salary
FROM employee;

SELECT AVG(salary) AS average_salary
FROM employee;

SELECT MAX(salary) AS max_salary
FROM employee;

SELECT MIN(salary) AS min_salary
FROM employee;

SELECT COUNT(*) AS num_of_employees
FROM employee;








3. Date Functions
-----------------
SELECT CURDATE();
SELECT year(doj) FROM employee;
SELECT empname, (YEAR(CURDATE())-YEAR(doj)  ) AS experience from employee;


SELECT empName, DATEDIFF(CURDATE(), doj) / 365 AS experience_in_years FROM employee;




--------------------------------------------------------------------------------


Operators (>, <, >=, <=, ==, !=)
--------------------------------

Select * from employee;
Select * from employee where empId=101;
select * from employee where salary > 5000;
select * from employee where salary > 5656.56;
select * from employee where salary >= 5656.56;
select * from employee where salary < 5656.56;
select * from employee where salary <= 5656.56;
select * from employee where gender='Male';
select * from employee where gender='Female';
select * from employee where emailId != 'harsha@gmail.com';
select * from employee where emailId != 'harsha@gmail.com';

select * from employee where salary > 5000;
select * from employee where salary > 5000 and gender='Male';
select * from employee where salary > 5000 or gender='Male';
select * from employee where salary > 5000 or gender='Female';


Aggregrate Functions
--------------------
sum()
min()
max()
count()
avg()


sum():
-----
select sum(salary) from employee;
select sum(salary) as 'Salary Investment' from employee;


min():
-----
select min(salary) from employee;

max():
-----
select max(salary) from employee;


count():
--------
select count(empid) from employee;
select count(empName) from employee;
select count(gender) from employee;


distinct
--------
select distinct(gender) from employee;
select count(distinct(gender)) from employee;


avg():
------
select avg(salary) from employee;
select sum(salary) from employee;
select sum(salary)/count(empId) from employee;




------------------------------------------------------------------



Order By
--------
Select * from employee order by salary;
Select * from employee order by salary asc;
Select * from employee order by salary desc;


------------------------------------------------------------------


Group By
--------

select sum(salary) from employee;
select sum(salary) from employee group by gender;
select gender, sum(salary) from employee group by gender;
select gender, avg(salary) from employee group by gender;


------------------------------------------------------------------

Between
-------
select * from employee where salary >= 4545.45 and salary <= 7878.78;
select * from employee where salary between 4545.45 and 7878.78;



------------------------------------------------------------------



IN and Not IN
-------------

select * from employee where empId=101 or empId=103 or empid=105 or empId=106;
select * from employee where empName='Harsha' or empName='Indira' or empName='Deepika' or empName='Utkarsh';

select * from employee where empId in (101, 103, 105, 106);
select * from employee where empId not in (101, 103, 105, 106);

select * from employee where empName in ('Harsha', 'Indira', 'Deepika', 'Utkarsh');
select * from employee where empName not in ('Harsha', 'Indira', 'Deepika', 'Utkarsh');



------------------------------------------------------------------



